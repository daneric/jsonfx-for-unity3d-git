﻿using System;
using System.Text;
using Pathfinding.Serialization.JsonFx;
using System.Collections.Generic;
using System.IO;

namespace Pathfinding.Serialization.JsonFx.Test.UnitTests
{
	public class DefaultAssembly
	{

		class A {
			public A childA;
			[JsonOrder(-1)]
			public B childB;
		}
			
		class B : A {
		}

		public static void RunTest (TextWriter writer, string unitTestsFolder, string outputFolder) {
			RunTestInternal (writer, unitTestsFolder, outputFolder, false);
			RunTestInternal (writer, unitTestsFolder, outputFolder, true);
		}

		static void RunTestInternal (TextWriter writer, string unitTestsFolder, string outputFolder, bool defaultAssembly) {
			JsonWriterSettings wsettings = new JsonWriterSettings();
			wsettings.PrettyPrint = true;
			wsettings.TypeHintName = "@type";
			wsettings.TypeHintsOnlyWhenNeeded = true;
			wsettings.DefaultAssembly = typeof(A).Assembly;

			typeof(A).Assembly.GetType ("Pathfinding.Serialization.JsonFx.Test.UnitTests.DefaultAssembly+A", true);

			var root = new A ();
			root.childA = new B ();
			root.childB = new B ();
			root.childB.childA = new B ();
			root.childA.childA = new B ();

			using (StreamWriter streamWriter = new StreamWriter(outputFolder+"/DefaultAssembly.json", false, Encoding.UTF8)) {
				JsonWriter wr = new JsonWriter(streamWriter, wsettings);
				wr.Write(root);
			}

			using (StreamReader re = new StreamReader (outputFolder + "/DefaultAssembly.json", Encoding.UTF8)) {
				JsonReaderSettings rsettings = new JsonReaderSettings ();
				rsettings.TypeHintName = wsettings.TypeHintName;
				rsettings.DefaultAssembly = wsettings.DefaultAssembly;
				rsettings.OnInvalidTypeHint = s => {
					throw new System.Exception ("Invalid type hint: '" + s + "'");
				};

				JsonReader read = new JsonReader (re, rsettings);

				var p = (A)read.Deserialize (typeof(A));
				if (p.childA.GetType () != typeof(B))
					throw new System.Exception ();

				if (p.childB.GetType () != typeof(B))
					throw new System.Exception ();

				if (p.childA.childA.GetType () != typeof(B))
					throw new System.Exception ();

				if (p.childB.childA.GetType () != typeof(B))
					throw new System.Exception ();
			}
		}
	}
}

